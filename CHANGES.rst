Changelog
=========

0.2.0b1 (2023-02-15)
--------------------
- Python support for version 3.10 3.11.
- Update project configurations and informations.

0.1.6 (2022-11-05)
------------------
- Updates related to changes in api of pycparser_util package (0.1.9).

0.1.5 (2022-10-25)
------------------
- Updates related to changes in api of pycparser_util package.

0.1.4 (2022-10-22)
------------------
- Moving c_str, ConverterAstToStr, Parser, FuncDeclGetter to separate package - pycparser-util.

0.1.3 (2022-10-08)
------------------
- Fix mocking function that returns pointer.

0.1.2 (2022-09-14)
------------------
- Multiple sources (h files) could be now passed as input.
- cmock.tools.get_require_package_names added.

0.1.1 (2022-09-11)
------------------
- Fixes: folders creation, destination generated path file fix.

0.1.0 (2022-09-11)
------------------
- Initial commit.
