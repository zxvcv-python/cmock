import sys
import logging
import argparse
from typing import List
from pathlib import Path

from .MockGenerator import MockGenerator

log = logging.getLogger(__name__)


def __add_required_parameters(parser:argparse.ArgumentParser) -> None:
    required_params = parser.add_argument_group("basic parameters")

    required_params.add_argument("source",
        type=Path,
        nargs='*',
        help="""Header file for which Mock should be generated."""
    )

    required_params.add_argument("--mock_file_name",
        type=str,
        required=True,
        help="""Name of the generated mock files:
                '<mock-file-name>.cpp' and '<mock_file_name>.hpp'"""
    )

    required_params.add_argument("--mock_suffix_name",
        type=str,
        required=True,
        help="""Suffix of generated Mock class file. It always starts with 'Mock'
                so target mock name will be: Mock<mock_suffix_name>."""
    )

    required_params.add_argument("--destination",
        type=Path,
        default=Path.cwd(),
        help="""Destination path where cpp and hpp file will be placed.
                Default to cwd."""
    )

    required_params.add_argument("--header-destination",
        type=Path,
        dest="header_destination",
        default=Path.cwd(),
        help="""If passed header file will be placed into this directory path instead
                of under 'destination'."""
    )

def __add_compiler_parameters(parser:argparse.ArgumentParser) -> None:
    required_params = parser.add_argument_group("compiler parameters")

    required_params.add_argument("--includes",
        type=str,
        nargs="*",
        default=[],
        help="""Include paths to be passed as parameter '-I<path>' into the compiler.
                Separated by spaces."""
    )

    required_params.add_argument("--defines",
        type=str,
        nargs="*",
        default=[],
        help="""Definitions to be passed as parameter '-D<definiton>' into the compiler.
                Separated by spaces."""
    )

    required_params.add_argument("--compiler-params",
        type=str,
        nargs="*",
        default=[],
        help="""Other custom parameters to be passed into the compiler.
                Separated by spaces."""
    )

def __add_logging_parameters(parser:argparse.ArgumentParser) -> None:
    required_params = parser.add_argument_group("logging parameters")

    required_params.add_argument("--log",
        type=str,
        default="WARNING",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
        help="""Set logging level for this package."""
    )

def __args_post_parsing(args) -> None:
    # includes post parsing
    args.includes = [Path(i) for i in args.includes]

    # defines post parsing
    defines = {}
    for d in args.defines:
        ds = d.split("=")
        defines[ds[0]] = None if len(ds) == 1 else ds[1]
    args.defines = defines

def main(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='Process some integers.')
    __add_required_parameters(parser)
    __add_compiler_parameters(parser)
    __add_logging_parameters(parser)
    args = parser.parse_args(argv)
    __args_post_parsing(args)
    # logging.setLevel(args.log) # TODO[PP]: to be fixed ad tested

    t = MockGenerator()
    t.generate(
        files=               args.source,
        destination=        args.destination,
        mock_file_name=     args.mock_file_name,
        mock_suffix_name=   args.mock_suffix_name,
        header_destination= args.header_destination,
        includes=           args.includes,
        defines=            args.defines,
        compiler_params=    args.compiler_params
    )

if __name__.rpartition(".")[-1] == "__main__":
    sys.exit(main(sys.argv[1:]))
