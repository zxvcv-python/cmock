#pragma once

\#include "gmock/gmock.h"

extern "C"
{
${headers_includes}
}

class Mock${mock_suffix_name} {
public:
    Mock${mock_suffix_name}() { mock = this; }
${methods_declaration}

    static Mock${mock_suffix_name}* mock;
};