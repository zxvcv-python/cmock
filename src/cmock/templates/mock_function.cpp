    ${function_type} ${function_name}(${params_declaration})
    {
        return Mock${mock_suffix_name}::mock->${function_name}(${params_names});
    }