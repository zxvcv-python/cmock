from pathlib import Path
from pycparser import c_ast
from Cheetah.Template import Template
from typing import List, Dict, Optional

from zxvcv.util.Path import here

from pycparser_util.Parser import Parser
from pycparser_util.c_objects import CFuncDecl
from pycparser_util.c_ast_getters import FuncDeclGetter


class MockGenerator():
    def __init__(self):
        with (here(__file__)/"templates/mock_function.cpp").open("r") as f:
            self.tplt_func_cpp = f.read()
        with (here(__file__)/"templates/mock_function.hpp").open("r") as f:
            self.tplt_func_h = f.read()

    def _generate_function_cpp(self, data:CFuncDecl, mock_suffix_name:str) -> str:
        parameters = []
        parameters_names = []
        for i, p in enumerate(data.params):
            if not p['name']:
                p['name'] = f"_param{i}"

            parameters.append(f"{p['type']} {p['name']}")
            parameters_names.append(p['name'])

        return Template(self.tplt_func_cpp, searchList=[{
            "function_type": data.type,
            "function_name": data.name,
            "params_declaration": ", ".join(parameters) if parameters else "",
            "params_names": ", ".join(parameters_names) if parameters else "",
            "mock_suffix_name": mock_suffix_name
        }])

    def _generate_function_h(self, data:CFuncDecl) -> str:
        parameters = []
        for i, p in enumerate(data.params):
            parameters.append(f"{p['type']} {p['name']}")

        return Template(self.tplt_func_h, searchList=[{
            "function_type": data.type,
            "function_name": data.name,
            "params_count": len(data.params),
            "params_declaration": ", ".join(parameters) if parameters else ""
        }])

    def generate(self, files:Path, destination:Path,
                 mock_file_name:str,
                 mock_suffix_name:str,
                 header_destination:Path = None,
                 includes:List[Path] = [],
                 defines:Dict[str, Optional[str]] = {},
                 compiler_params:List[str] = []):
        # create directories
        destination.mkdir(parents=True, exist_ok=True)
        header_destination.mkdir(parents=True, exist_ok=True)

        files_to_generate = [
            {
                "tplt": "mock_file.cpp",
                "name": f"{mock_file_name}.cpp",
                "destination": destination,
                "data": {
                    "mock_suffix_name": mock_suffix_name,
                    "mock_header_file": f"{mock_file_name}.hpp",
                    "methods_definitons": []
                }
            },
            {
                "tplt": "mock_file.hpp",
                "name": f"{mock_file_name}.hpp",
                "destination": header_destination if header_destination else destination,
                "data": {
                    "mock_suffix_name": mock_suffix_name,
                    "headers_includes": "\n".join([f"    #include \"{f.name}\"" for f in files]),
                    "methods_declaration": []
                }
            }
        ]

        # files parsing
        for file in files:
            ast = Parser().parse_file(file, includes, defines, compiler_params)
            fd_get = FuncDeclGetter()
            fd_get.visit(ast)

            for i in fd_get.items:
                generated_cpp = str(self._generate_function_cpp(CFuncDecl(i), mock_suffix_name))
                generated_hpp = str(self._generate_function_h(CFuncDecl(i)))

                files_to_generate[0]["data"]["methods_definitons"].append(generated_cpp)
                files_to_generate[1]["data"]["methods_declaration"].append(generated_hpp)

        files_to_generate[0]["data"]["methods_definitons"] = \
            "\n\n".join(files_to_generate[0]["data"]["methods_definitons"])
        files_to_generate[1]["data"]["methods_declaration"] = \
            "\n".join(files_to_generate[1]["data"]["methods_declaration"])

        # mock generation
        for file in files_to_generate:
            with (here(__file__)/f"templates/{file['tplt']}").open("r") as f:
                template = f.read()

            code = str(Template(template, searchList=[file['data']]))
            with (file["destination"]/file["name"]).open("w") as f:
                f.write(code)

    def generate_function_mock(self):
        pass

    def generate_file_mock(self):
        pass

    def generate_component_mock(slef):
        pass
