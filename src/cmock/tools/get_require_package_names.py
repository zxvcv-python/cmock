import re
import sys

from typing import List
from pathlib import Path


def get_require_package_names(conaninfo_file:Path) -> List[str]:
    with conaninfo_file.open("r") as f:
        data = f.readlines()

    in_requires_section = False
    requires = []

    for i, line in enumerate(data):
        requires_mach = re.search('\[requires\]', line)
        section_mach = re.search('\[*\]', line)

        if requires_mach and section_mach: in_requires_section = True
        if not requires_mach and section_mach and in_requires_section: in_requires_section = False

        if in_requires_section and not requires_mach:
            requires.append(line.strip())

    return [i.split("/")[0] for i in requires if i]

def main(argv=sys.argv[1:]):
    conaninfo_file = Path(argv[0])
    output = get_require_package_names(conaninfo_file)
    print(" ".join(output), end="")

if __name__.rpartition(".")[-1] == "__main__":
    # TODO[PP]: move it as a 'cmkae.tool get_require...'command run
    sys.exit(main(sys.argv[1:]))
