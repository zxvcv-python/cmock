# cmock
Toolbox for generating google mocks for C code.

# Build
Build all with single command run:
```
tox
```

Build wheel for specific python:
```
tox -e py310
tox -e py311
```

# Publish
> require tokens for pypi and testpypi in ~/.pypirc file

### publish on pypi
```
tox -e publish
```

### publish on testpypi
```
tox -e publish-test
```
